SDL makes a great companion library to cairo. With SDL, you can create
portable multimedia applications very easily. Install the latest SDL
development packages for your distribution, or download the latest SDL
development kit from [http://www.libsdl.org](http://www.libsdl.org/).

Drawing with cairo to an SDL Surface is quite simple, as all you need
to do is create a `cairo_surface_t` from the SDL_Surface using
`cairo_image_surface_create_for_data()`.  This works as expected as
long as SDL and cairo agree on the pixel format.  For example:

	SDL_Surface *sdlsurf = SDL_CreateRGBSurface (
		flags, width, height, 32,
		0x00FF0000, /* Rmask */
		0x0000FF00, /* Gmask */
		0x000000FF, /* Bmask */
		0); /* Amask */

	/* ... make sure sdlsurf is locked or doesn't need locking ... */

	cairo_surface_t *cairosurf = cairo_image_surface_create_for_data (
		sdlsurf->pixels,
		CAIRO_FORMAT_RGB24,
		sdlsurf->w,
		sdlsurf->h,
		sdlsurf->pitch);

	/* ... normal cairo calls ... */


All the usual SDL locking rules apply, so the `pixels` member of your
`SDL_Surface` needs to be valid and stay put for the lifetime of the
cairo surface.

SDL's 32 bit pixel formats with per pixel alpha are not supported
directly by cairo because cairo and SDL disagree on the interpretation
of pixel values with alpha: SDL uses unpremultiplied pixels but cairo
uses premultiplied pixels.  However, using
[cairosdl](http://cgit.freedesktop.org/~joonas/cairosdl/) drawing to
such `SDL_Surface`s is almost as straightforward as to any other cairo
surface; chiefly the difference is that one needs to flush the surface
before the results appear in the `SDL_Surface`.  You can also use cairo
to composite a cairo surface with transparency to an `SDL_Surface`
without, and thus sidestep the problem entirely.

Examples of using SDL with cairo are available in the
[demos](http://cgit.freedesktop.org/~joonas/cairosdl/tree/?h=demos)
branch of cairosdl.
