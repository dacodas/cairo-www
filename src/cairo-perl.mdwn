[[!meta title="cairo-perl &mdash; Perl bindings for cairo"]]

cairo-perl provides a convenient and [Perlish](http://www.perl.org/) interface to the cairo graphics engine.

# Download

cairo-perl is available from the [cairo-perl SVN module](http://svn.gnome.org/viewvc/perl-Cairo/) over at GNOME.  To check it out, use:

    svn co svn://svn.gnome.org/svn/perl-Cairo/trunk perl-Cairo

Released versions are available from [CPAN](http://search.cpan.org/dist/Cairo/).  If you have CPAN.pm set up, you can install cairo-perl with

    cpan Cairo

cairo-perl is available under the terms of the LPGL version 2.1 or later.

# Documentation

The module itself contains an API listing that is available in [HTML form on CPAN](http://search.cpan.org/dist/Cairo/lib/Cairo.pm) or in POD after you have installed cairo-perl:

    perldoc Cairo

For more information, refer to the [cairo manual](http://cairographics.org/manual/).  Most of its content is directly applicable to the Perl bindings as well.

# Contact

For general help with using cairo and its Perl bindings, use the [cairo mailing list](http://lists.cairographics.org/cgi-bin/mailman/listinfo/cairo).  If the question clearly only concerns the Perl bindings, a better place might be the [gtk-perl mailing list](http://mail.gnome.org/mailman/listinfo/gtk-perl-list) over on gnome.org.
